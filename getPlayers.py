from lxml import etree

playerList = []

def addPlayer(uid):
	return playerList.append(uid)

# def addPlayer(playerinfo):
# 	player = {}
# 	for item in playerinfo:
# 		player[item] = playerinfo[item]

# 	return playerList.append(player)

def addInfoToPlayer(uid,playerinfo):
	playerlist[uid] = {

	}
	for uid in playerList:
		for item in playerinfo:
			uid[item] = playerinfo[item]


# get player's unique id's based on chosen server(s)/global, country, map, city
def getPlayersUniqueIds(servers,filters):
	gameType = "tf2"
	#target specific servers vs global
	#filter types are uniqueid/,name/,weapon/,country/,city/

	api = "http://api.gameme.net/playerlist/"+gameType
	tree = etree.parse(api)
	uids = []

	uniqueidtags = tree.xpath('/gameME/playerlist/player/uniqueid')
	for steamid in uniqueidtags:
		#dictSteamId = {steamid.text:}
		uids.append(steamid.text)

	return uids

# get players based on inputted play time and associate with unique id, 
# favorite weapon, city and country, steam avatar, and name.
# http://api.gameme.net/playerinfo/tf2/STEAM_0:0:7072743/status
def getPlayersInfo(uids,playtime):
	#set less than or greater then on playtime
	uidsWithPlayTime = [{}]
	uidsPlayTime = []
	uidList = ""
	for uid in uids:
		uidList = uidList+"/"+uid


	api = "http://api.gameme.net/playerinfo/tf2"+uidList
	tree = etree.parse(api)
	time = tree.xpath('/gameME/playerinfo/player/time')

	for t in time:
		uidsPlayTime.append(t.text)

	return uidsWithPlayTime

# get players # of hours, online status.
# filter by desired item, online status, number of hours
def getPlayersSteamInfo():
	return 0

def getPlayersDisplay():
	return 0

#print(getPlayersInfo(getPlayersUniqueIds(0,1),0))
print(getPlayersUniqueIds(0,1))
#print(getPlayersInfo(0,1))

# playerinfo = {"time":111,"poo":"yes"}
# addPlayer(playerinfo)
# print (playerList)